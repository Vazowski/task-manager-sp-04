package ru.ermakov.taskmanager.model;

import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;

import javax.persistence.*;

@Entity
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractDeal {

    @ManyToOne
    @JoinColumn(name = "projectId")
    private Project project;

    public Task() {
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
