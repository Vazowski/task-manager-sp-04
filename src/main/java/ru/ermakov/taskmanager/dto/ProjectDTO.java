package ru.ermakov.taskmanager.dto;

public class ProjectDTO extends AbstractDealDTO {

    public ProjectDTO() {

    }

    @Override
    public String toString() {
        return "ID: " + this.getId() +
                "\nName: " + this.getName() +
                "\nDescription: " + this.getDescription() +
                "\nDateCreated: "+ this.getDateCreated() +
                "\nDateBegin: " + this.getDateBegin() +
                "\nDateEnd: " + this.getDateEnd() +
                "\nStatus: " + this.getReadinessStatus();
    }
}